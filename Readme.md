# Facial Recognition Attendance Management System

### NB: 
```
As a result of the facial recognition modules used in development of this system it does not run on an online server. 
It is recommended that you run this program locally in a virtualized environment using pipenv. See https://pypi.org/project/pipenv/ for more information on pipenv and how to get started
There is also a video of the working system in the repository
```

## Requirements
* Python
* Flask
* MYSQL


## Installation 
```
Clone the repo
cd facialdetectionmodel
launch students.sql to install database
Visit the Instruction Manual.txt for specific steps and further detail on installation 
Thank you
```
## Installing Facial Recognition
* Download the latest version Microsoft Build Tools
* See Instruction manual for more details

## Post Installation
```
cd facialdetectionmodel
run main.py
Thank you
```
## Database
Install the lastest version of mysql server
https://www.mysql.com/downloads/

##HOST LINK
* facialattendancesystem.herokuapp.com
