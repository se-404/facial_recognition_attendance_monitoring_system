import face_recognition
from flask import Flask, jsonify, request, redirect, render_template

# You can change this to any folder on your system
ALLOWED_ = {'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)


def allow_ext(file_):
    return '.' in file_ and file_.rsplit('.', 1)[1].lower() in ALLOWED_


@app.route('/', methods=['GET', 'POST'])
def upload():

    if request.method == 'POST':
        if 'file' not in request.files:
            return redirect(request.url)

        file = request.files['file']

        if file.filename == '':
            return redirect(request.url)

        if file and allowed_file(file.filename):

            return detect_faces_in_image(file)

    #return render_template('upload image page')

@app.route('/detect', methods=['GET', 'POST'])
def detect_faces(file_stream):
    pass


