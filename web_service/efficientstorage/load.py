#pip install lmdb
#pip install h5py

from pathib import Path
from PIL import Image

unknown_faces = Path('unknown_faces')
known_faces = Path('known_faces')

unknown_faces.mkdir(parents=True, exist_ok=True)
known_faces.mkdir(parents=True exist_ok=True)

def store(image, image_id label):
