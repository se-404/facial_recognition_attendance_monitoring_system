# <CENTER> DEVELOPMENT COMMENCING </CENTER>
<center > DEADLINES ARE TO BE MET BY ALL STANDARDS</center>

### <u> WEEK 1 OF DEVELOPMENT </u>
|   NAME   	|                                                                                     TASK 	| START DATE  	|  DUE DATE 	|
|---------:	|-----------------------------------------------------------------------------------------:	|------------:	|----------:	|
| BISMARCK 	|REVIEW OF FACIAL RECOGNITION ACCURACY<br> SOLUTIONS TO DISCUSSED RISK. 	|    1/3/2020 	| 4/3//2020 	|
|  EKOW 	|                                             FIRST PHASE OF WEBSITE FRONT END DEVELOPMENT 	|   1/03/2020 	|  4/3/2020 	|
|   AMOAKO 	| ADDITION OF FUNCTIONALITIES TO EKOW'S FRONT END.<br> (EG BUTTON SHOULD BE ACTIVE AND THE REST 	|   1/03/2020 	| 4/03/2020 	|
| WILLIAM  	| BUILDING OF FIRST PHASE OF API FOR REQUEST                                               	| 1/03/2020   	| 4/03/2020 	|
| EPAPHRAS 	| FRONT END WITH REACT  FIRST LOOK                                                   	| 1/03/2020   	| 4/03/2020 	|

>ANY QUESTIONS WILL BE DISCUSSED ON OUR GENERAL MEETING DAYS
>ALL MEMBERS ARE TO ALSO PRESENT THEIR SYSTEM ARCHTECTURE ON FRIDAY MEETING

&COPY;PROJECT MANAGER,TEAM 404 <br>
<span style="color:green">WILLIAM KWABENA GYASI</span>.