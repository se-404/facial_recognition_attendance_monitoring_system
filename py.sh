#!/bin/bash


echo Welcome to the Facial Recognition Attendance System, sit back while we install the packages you need

echo installing pipenv

pip3 install pipenv

echo Installed pipenv

pipenv install Flask==1.1.2

echo activating virtual environment
pipenv shell

echo You are good to go
