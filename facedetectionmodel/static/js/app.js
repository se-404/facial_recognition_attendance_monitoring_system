var message = null;
var active_section = null;
var train_data={
    name:"",
    file:null
}
var date=new Date().toDateString();



$('#querytable').on('click',function (event) {


})




function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result);
                    document.querySelector("#take-attendance").style.display="block";
                };
                reader.readAsDataURL(input.files[0]);
            }

}

function studentUpload(input){
    if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#student_image')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                train_data.file=input.files[0]
            }
}

 function generateTable(data){
    var col = [];
        for (var i = 0; i < data.length; i++) {
            for (var key in data[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }

        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");

        var tr = table.insertRow(-1);                   // TABLE ROW.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i];
            tr.appendChild(th);
        }

        // ADD JSON DATA TO THE TABLE AS ROWS.
        for (var i = 0; i < data.length; i++) {

            tr = table.insertRow(-1);

            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = data[i][col[j]];
            }
        }

        var divContainer = document.getElementById("query-show");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);

 }


$(document).ready(function() {

	$('#recognize').on('submit', function(event) {
		 var form_data = new FormData($('#recognize')[0]);
		$.ajax({
            type: 'POST',
            url: '/api/recognize',
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
                 $('.modal-spinner').modal('show');
       setTimeout(function () {
           document.querySelector(".spinner-changes").textContent="Re-training Model"
       },1500)
         setTimeout(function () {
           document.querySelector(".spinner-changes").textContent="Performing Matching Sequence"
       },3000)
            },
            success: function(data) {
                if(data.number_of_faces >0){
                    document.querySelector(".results-p").textContent=`Our Model Identified And Recorded ${data.number_of_faces} Students`
                     document.getElementById('main_place').innerHTML = document.getElementById('replace').innerHTML;
                    generateTable(data.querydata)
                    console.log(data.number_of_faces)
                    console.log(data.querydata)
                    setTimeout(function () {
                         document.querySelector('#preview-img').src = '../../static/img/model_complete.jpg'

                    },300)

                }
                 $('.modal-spinner').modal('hide');
                },
        })
		.done(function(data) {

		});

		event.preventDefault();
	});



    //LECTURER REGISTRATION
	$('#lecturer_register').on('submit', function(event) {
	    console.log("ad")
	    const first_name = document.getElementById('inputFirst').value
        const last_name = document.getElementById('inputLast').value
        const email = document.getElementById('inputEmail').value
        const password = document.getElementById('inputPassword').value
        const confirmpassword = document.getElementById('confirmPassword').value
        const phonenumber = document.getElementById('phonenumber').value
        const op_value = document.getElementById('depart')
        const selected_depart = op_value.options[op_value.selectedIndex].value
        $.ajax({
            type: 'POST',
            url: '/lecturer_registration',
            data: {
                firstname:first_name,
                lastname:last_name,
                email:email,
                password:password,
                phonenumber:phonenumber,
                department:selected_depart
            },
            beforeSend: function () {

            },
            success:function (data) {
                if(!data.success){
                    console.log("error_code")
                }
                if(data.success){
                    window.location.href="take_attendance"
                     console.log(data)
                }


            }

        })
            .done(function (data) {
                
            })
      event.preventDefault();
    })


    //UPLOAD PICTURER
    document.getElementById('get_file').onclick = function() {
	    document.getElementById('my_file').click();
	};


	//DATE PICKER INITIALIZER



})




jQuery(document).ready(function () {

    var student_firstname=document.querySelector("#student_firstname").value
    var student_lastname=document.querySelector("#student_lastname").value
    var student_indexnumber=document.querySelector("#student_indexnumber").value
    var student_email=document.querySelector("#student_email").value
    var student_password=document.querySelector("#student_password").value

    var op_value = document.getElementById('depart')
    var selected_depart = op_value.options[op_value.selectedIndex].value

    var year_student = document.getElementById('student_year')
    var student_year = year_student.options[year_student.selectedIndex].value

    var student_picture = document.getElementById('student_registration');



    $('#student_registration fieldset:first-child').fadeIn('slide');

    //NEXT BUTTON
    $('#student_registration .btn-next').on('click', function () {
        var parent_fieldset = $(this).parents('fieldset');
        var next_step = true;
        parent_fieldset.find('input[type="text"],input[type="email"], input[type="password"],input[type="number"],input[name="student_department"],input[name="student_year"]').each(function () {
            if ($(this).val() == "") {
               $(this).addClass('input-error');
    			next_step = false;
            } else {
                  student_firstname=document.querySelector("#student_firstname").value
                student_lastname=document.querySelector("#student_lastname").value
                student_indexnumber=document.querySelector("#student_indexnumber").value
                student_email=document.querySelector("#student_email").value
                student_password=document.querySelector("#student_password").value
                op_value = document.getElementById('depart')
                selected_depart = op_value.options[op_value.selectedIndex].value

                  year_student = document.getElementById('student_year')
                 student_year = year_student.options[year_student.selectedIndex].value

                console.log(student_year)
                $(this).removeClass('input-error');
            }
        });
    if( next_step ) {
        console.log('eill')
console.log(student_year)
    		parent_fieldset.fadeOut(400, function() {
	    		$(this).next().fadeIn();
	    	});
    	}

    });

     $('#student_registration .btn-previous').on('click', function() {
         console.log(student_year)
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).prev().fadeIn();
    	});
    });


     $('#student_registration').on('submit', function(e) {
    	$(this).find('input[type="text"],input[type="email"], input[type="password"],input[type="number"],input[name="student_department"],input[type="file"]').each(function() {
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    		      var train_form_data = new FormData();
    		      train_data.name=`${student_indexnumber}`
            train_form_data.append('form', train_data.name);
            train_form_data.append('file', train_data.file);
             train_form_data.append('firstname', student_firstname);
             train_form_data.append('lastname', student_lastname);
             train_form_data.append('year', student_year);
             train_form_data.append('department', selected_depart);
             train_form_data.append('index_number',student_indexnumber)
                train_form_data.append('email',student_email)
                train_form_data.append('password',student_password)
            console.log(train_form_data)
    		    $.ajax({
                    type: 'POST',
                    url: '/api/train',
                    data: train_form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend:function(){
                        console.log('sendinginnnnnnnggg')
                    },
                     success:function (data) {
                        if(!data.success){
                            console.log("error_code")
                }
                        if(data.success){
                            window.location.href="student_homepage"
                            console.log(data)
                }


            }
                })
                    .done(function () {

                    })
    		}
    	});

    });


})




$('#datepicker').datepicker({
    maxDate: new Date(),
    dateFormat:"yy-mm-dd",
    defaultDate:new Date(),
});
$('#querytable').on('click',function(){
    var assign= new Date()
    assign=$('#datepicker').datepicker( "getDate" )
    if(assign===null){
    document.querySelector("#error-message").style.display='block';
    }
    else{
         var query_data = new FormData();
         var month = assign.getUTCMonth() + 1; //months from 1-12
var day = assign.getUTCDate();
var year = assign.getUTCFullYear();
const newdate = year + "-" + month + "-" + day;
         query_data.append('date', newdate);
         $.ajax({
             type: 'POST',
             url: '/view',
             data: query_data,
             contentType: false,
             cache: false,
             processData: false,
             beforeSend:function () {
                 console.log("Fetchingggg")
             },
             success:function (data) {
                 if(data.success){
                     document.getElementById('queryresults').style.display ='block';
                        document.getElementById('nodata').style.display ='none';
                     generateTable(data.query)
                 }

                 else{
                        document.getElementById('queryresults').style.display ='none';
                        document.getElementById('nodata').style.display ='block';

                     console.log("Data Found")

                 }
             }
         })
             .done(function () {
                 
             })


    }
    console.log("adad")
    console.log(assign.toDateString())
})















