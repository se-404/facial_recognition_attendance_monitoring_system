from flask import Flask,request,jsonify,json,Response,render_template,redirect,url_for,session
from werkzeug.utils import secure_filename
from os import path, getcwd
app=Flask(__name__)
import time
from datetime import datetime
from studentdatabase import StudentDatabase
from face import FacialRecognition
from lecturer_db_login import LecturerLogin
#DATE TIME OBJECT
app.config['file_allowed'] = ['image/png', 'image/jpeg']
app.config['storage'] = path.join(getcwd(), 'storage')
app.db = StudentDatabase()
app.lecturer_login=LecturerLogin()
app.facialrecognition=FacialRecognition(app)
app.secret_key='Team404'
fetch_identify_keys=0
#WHEN REQUEST IS SUCCEFULLY MADE


# matched_students={}
# now = datetime.now()
# formatted_date = now.strftime('%Y-%m-%d')
# print(formatted_date)
# fetch_data = """SELECT studentname,student_index_number,state,date_taken,programme,year FROM attendance JOIN student_details ON student_details.id=attendance_id WHERE date_taken=%s """
# query_results = app.db.select(fetch_data,[formatted_date])
# print(query_results)
#



def sucess_handler(output,status=200,mimetype='application/json'):
    return Response(output,status=status,mimetype=mimetype)
#WHEN REQUEST RETURNS AN ERROR
def error_handler(error,status=500,mimetype='application/json'):
    return Response(json.dumps({
        "error":{"error Message":error}
    }) ,status=status,mimetype=mimetype)

#FETCH STUDENT DETAILS FROM DATABASE
def get_student_by_id(student_id):
    student ={}
    select_query = """ SELECT student_details.id,studentname,index_number,createdAt,faces.id,encoding,filename
     FROM student_details LEFT JOIN faces ON faces.id = student_details.id  
     WHERE student_details.id= %s"""
    results = app.db.select(select_query,[student_id])
    index =0
    for row in results:
        face ={
            "id":row[4],
            "encoding":row[5],
            "filename":row[6],
        }
        if (index ==0):
            student ={
                'id':row[0],
                'name':row[1],
                'created':row[3],
                'faces':[]
            }
            if "id" in student:
                student['faces'].append(face)
        index =index+1
    return student

def delete_student_by_id(student_id):
    #DELETE STUDENT FROM DATABASE
    app.db.delete('DELETE FROM studentdetails WHERE id=?',[student_id])
    #DELETE PICTURE REFERENCE
    app.db.delete('DELETE FROM faces WHERE id=?',[student_id])
    print('STUDENT DELETED')


@app.route('/',methods=["GET"])
def homepage():
    return render_template("admindashboard.html")

@app.route('/index_page')
def index_page():
    return render_template("index.html")

@app.route('/admin_login')
def admin():
    return render_template("admin_login.html")

#Check API status / version
@app.route('/api/status')
def api_status():
    output =json.dumps({
        "api" :"1.0"
    })
    resp = {'feedback': "msg", 'category': "category"}
    return sucess_handler(output)

@app.route('/test_endpoint',methods=['POST'])
def test_endpoint():
    email = request.form['email']
    number = request.form['number']
    if email and number:
        newName = email[::-1]
        print(email)
        return jsonify({'email': email})
    return jsonify({'error': 'Missing data!'})






@app.route('/enroll_student')
def enroll_student():
     return render_template('enroll_student.html', )
     if 'loggedin' in session:
          return render_template('enroll_student.html',)
#     #     # User is not loggedin redirect to login page
     return redirect(url_for('lecturerlogin'))






########### LECTURER #####################################
@app.route('/lecturerlogin',methods=['POST'])
def lecturerlogin():
    error_message=""
    if request.method =='POST' and 'email' in request.form and 'password' in request.form:
        print("Reached")
        email=request.form['email']
        password = request.form['password']
        query="""SELECT * FROM lecturer WHERE email = %s AND password = %s"""
        account =app.lecturer_login.login(query,[email,password])

        if account:
            session['loggedin'] = True
            session['id'] = account[0]
            session['username'] = account[1]
            session['department'] = account[5]
            # Redirect to home page
            print(account)
            return redirect(url_for('lecturerhomepage'))
        else:
            error_message = "Incorrect username/password!"

    return render_template('index.html',error_message=error_message)

@app.route('/take_attendance')
def lecturerhomepage():
    if 'loggedin' in session:
        username = session['username']
        department=session['department']
        return render_template('lecturer_dashboard.html',name =username,department=department)
        # User is not loggedin redirect to login page
    return redirect(url_for('lecturerlogin'))

@app.route('/view_attendance')
def view_attendance():
    if 'loggedin' in session:
        username = session['username']
        department = session['department']
    return render_template('view_attendance.html',name =username,department=department)

@app.route('/lecturer_profile',methods=['GET','POST'])
def lecturer_profile():
    if request.method == 'POST':
        return jsonify({"ad"})
    if 'loggedin' in session:
        username = session['username']
        id=session['id']
        department = session['department']
        profile_query="""SELECT * FROM lecturer WHERE id=%s"""
        results=app.lecturer_login.login(profile_query,[id])
    return render_template("lecturer_profile.html",name =username,department=department)

@app.route('/lecturer_signup')
def lecturer_sign_up():
    return render_template("lecturer_signup.html")

@app.route('/lecturer_registration',methods=['POST'])
def lecturer_registration():
    if request.method == 'POST' and 'email' in request.form and 'password' in request.form:
        # Create variables for easy access
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email=request.form['email']
        password=request.form['password']
        phonenumber=request.form['phonenumber']
        department=request.form['department']
        fullname=firstname+' '+lastname
        selection_query="""SELECT * FROM lecturer WHERE email = %s AND password = %s"""
        account = app.lecturer_login.register(selection_query,[email,password])
        if account:
            return ({"message":"Account Exist Already!","success":False})
        else:
            insertion_query="""INSERT INTO lecturer(fullname, password, email,phonenumber,department)
                                        VALUES (%s, %s, %s,%s,%s) """
            insertion_id=app.lecturer_login.insert(insertion_query,[fullname,password,email,phonenumber,department])
            if insertion_id:
                session['loggedin'] = True
                session['id'] = insertion_id
                session['username'] = fullname
                session['department'] = department
                print(insertion_id)
                return jsonify({"success": True})
    return jsonify({"success":False})



############STUDENT####################################
@app.route('/studentlogin',methods=['GET','POST'])
def studentlogin():
    error_message=""
    if request.method =='POST' and 'indexnumber' in request.form and 'password' in request.form:
        index_number=request.form['indexnumber']
        password = request.form['password']
        query="""SELECT * FROM student_details WHERE index_number = %s AND password = %s"""
        account =app.lecturer_login.login(query,[index_number,password])
        if account:
            session['loggedin'] = True
            session['id'] = account[0]
            session['username'] = account[1]
            print(account)
            # Redirect to home page
            return redirect(url_for('studenthomepage'))
        else:
            error_message = "Incorrect username/password!"

    return render_template('index.html',error_message=error_message)


@app.route('/student_homepage')
def studenthomepage():
    if 'loggedin' in session:
        username = session['username']
        department = session['department']
        return render_template('student_dashboard.html', name=username, department=department)
        # User is not loggedin redirect to login page
    return redirect(url_for('index_page'))
@app.route('/student_complaint')
def student_complaint():
    return render_template('student_complaint.html')
@app.route('/student_profile')
def student_profile():
    return render_template('student_profile.html')

@app.route('/add_student')
def admin_add_student():
    return render_template('admin_add_student.html')

@app.route('/add_lecturer')
def admin_add_lecturer():
    return render_template('admin_add_lecturer.html')

@app.route('/view_lecturer')
def admin_view_lecturer():
    return render_template('admin_view_lecturer.html')

@app.route('/view_student')
def admin_view_student():
    return render_template('admin_view_student.html')




@app.route('/student_sign_up')
def student_sign_up():
        return render_template("student_signup.html")

@app.route('/lecturer_registration',methods=['GET','POST'])
def student_registration():
    if request.method == 'POST':
        # Create variables for easy access
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email=request.form['email']
        print(firstname,lastname,email)
        return jsonify({"SPeed":"Speed Racing"})
    return jsonify({"error":"Error"})







@app.route('/lecturerlogout')
def lecturer_logout():
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
    session.pop('department',None)
    # Redirect to login page
    return redirect(url_for('index_page'))

@app.route('/studentlogout')
def student_logout():
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
    session.pop('department',None)
    # Redirect to login page
    return redirect(url_for('index_page'))


@app.route('/adminlogout')
def admin_logout():
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
   
    # Redirect to login page
    return redirect(url_for('index_page'))








#LECTURER TO TRAIN MODEL WHEN NEW IMAGES ARE ADDED
@app.route('/api/train',methods=['POST'])
def train():
    if request.method=='POST':
        print("arrival")
        output = json.dumps({"success": True})
        if 'file' not in request.files:
            print("Face image is required")
            return error_handler("Face image is required.")
        else:
            print("File request", request.files)
            file = request.files['file']
            if file.mimetype not in app.config['file_allowed']:
                print("File extension is not allowed")
                return error_handler("We are only allow upload file with *.png , *.jpg")
            # WHEN FILE EXIST IN API CALL
            else:
                # get name in form data
                name = request.form['form']
                print("Information of that face", name)
                print("File is allowed and will be saved in ", app.config['storage'])

                filename = secure_filename(file.filename)
                trained_storage = path.join(app.config['storage'], 'trained')
                file.save(path.join(trained_storage, filename))

                #Variables for insertion
                student_name = request.form['firstname']
                student_indexnumber=request.form['index_number']
                student_year=request.form['year']
                student_department=request.form['department']
                student_email=request.form['email']
                student_password=request.form['password']
                insert_query = """INSERT INTO student_details(studentname, index_number, student_year,department,email,student_password)
                                        VALUES (%s, %s, %s, %s, %s, %s) """
                user_id = app.db.insert(insert_query, [student_name, student_indexnumber,student_year,student_department,student_email, student_password])
                print(user_id)
                if user_id:
                    print('true')
                    print('User Saved in Data', student_indexnumber, user_id)
                    # user has been saved into database we pass the user id into the face database
                    insert_query = """INSERT INTO faces(encoding, filename) VALUES (%s, %s) """
                    face_id = app.lecturer_login.insert(insert_query, [user_id, filename])
                    if face_id:
                        session['loggedin'] = True
                        session['id'] = user_id
                        session['username'] = student_name
                        session['department']=student_department
                        return jsonify({"success":True,"message":"Welcome"})
                    else:
                        error_handler('Error Saving Face')
                        return error_handler("An error inserting Face")
                else:
                    print('Something Happened')
                    return jsonify({"success":False,"message":"Trouble Saving Data "})





# #GET STUDENT USER PROFILE
# @app.route('/api/students/<int:student_id>', methods=['GET','DELETE'])
# def student_profile(student_id):
#     if request.method == 'GET':
#         student = get_student_by_id(student_id)
#         if student:
#             return sucess_handler(json.dumps(student), 200)
#         else:
#             return  error_handler('Student Not Found',404)


    # if request.method =='DELETE':
    #     delete_student_by_id(student_id)
    #     return sucess_handler(json.dumps({"delete":True}))


@app.route('/view',methods=['POST'])
def view_attendance_query():
    if request.method=='POST':
        ##QUERY AS A LECTURER
        if 'date' in request.form:
            date=request.form['date']
            print(date)
            selection_query_trail="""SELECT * FROM attendance WHERE date_taken=%s"""
            selection_query="""SELECT studentname,index_number,state,date_taken,department,student_year FROM attendance JOIN student_details ON student_index_number=index_number WHERE date_taken=%s;"""
            results= app.db.query(selection_query_trail,[date])
            print(results)
            if results:
                return jsonify({"success":True,"query":results})
                print(results)
            return jsonify({"success": False})





#Router for unknown faces
@app.route('/api/recognize',methods=['POST'])
def recognize():
    if request.method=='POST':
        if 'file' not in request.files:
            return error_handler('Image is Required To Be Trained')
        else:
            file = request.files['file']
            # File Extension Validation
            if file.mimetype not in app.config['file_allowed']:
                return error_handler('File Extension invalid')
            else:
                filename = secure_filename(file.filename)
                unknown_storage = path.join(app.config['storage'], 'unknown')
                file_path = path.join(unknown_storage, filename)
                file.save(file_path)
                logged_lecturer= session['id']
                identified_faces = app.facialrecognition.recognize(filename,logged_lecturer)
                print(f"i found {int(identified_faces)} in this picture")
                if (int(identified_faces)>0):
                    now = datetime.now()
                    formatted_date = now.strftime('%Y-%m-%d')
                    print(formatted_date)
                    fetch_identify_keys = int(identified_faces)
                    fetch_data="""SELECT studentname,index_number,state,date_taken,department,student_year FROM attendance JOIN student_details ON student_index_number=index_number WHERE date_taken=%s;"""
                    query_results=app.db.select(fetch_data,[formatted_date])
                    print(query_results)
                    print("JSON HAS RETURNED")
                    # return redirect(url_for('attendance_screen'))
                    return jsonify({"number_of_faces":int(identified_faces),"querydata":query_results})
                else:
                   return jsonify({"error":"We encountered an error "})








if __name__ == "__main__":
    app.run(debug=True)
