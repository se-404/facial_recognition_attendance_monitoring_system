import mysql.connector as mysql
mydb = mysql.connect(
    host="facialattendancesystem.cppjsaukmy1z.eu-west-1.rds.amazonaws.com",
    user="admin",
    password="william123",
    database="fras"
)

class StudentDatabase:
    def __init__(self):
        self.connection = mydb
    def query(self, q, arg=()):
        cursor = self.connection.cursor(dictionary=True)
        cursor.execute(q, arg)
        results = cursor.fetchall()
        cursor.close()
        return results

    def fetch(self,q):
        cursor =self.connection.cursor()
        cursor.execute(q)
        results=cursor.fetchall()
        cursor.close()
        return results

    def insert(self, q,arg=()):
        cursor = self.connection.cursor()
        cursor.execute(q,arg)
        result = cursor.lastrowid
        mydb.commit()
        cursor.close()
        return result

    def select(self, q, arg):
        cursor = self.connection.cursor(dictionary=True)
        cursor.execute(q, arg)
        result = cursor.fetchall()
        return result

    def delete(self,q,arg=()):
        cursor = self.connection.cursor()
        result = cursor.execute(q,arg)
        mydb.commit()
        return result


