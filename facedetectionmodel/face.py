import face_recognition
from PIL import Image, ImageDraw
import os
from datetime import datetime
from os import path
from studentdatabase import StudentDatabase
student_database= StudentDatabase()
class FacialRecognition:
    def __init__(self,app):
        self.storage =app.config["storage"]
        self.db =app.db
        self.known_encoding_faces = []  # faces data for recognition
        self.face_user_keys = {}
        self.filenames=[]
        self.matched_index_numbers=[]
        self.faces=[]
        self.identified_face=0
        self.directory='../facedetectionmodel/storage/trained'


    # def load_train_file_by_name(self):
    #     for filename in os.listdir(self.directory):
    #         self.filenames.append(filename)
    #         print(self.filenames)
    #         trained_images = os.path.join(self.directory, filename)
    #         # trained_storage = path.join(self.storage, 'trained')
    #         return trained_images

    def load_unknown_file_by_name(self, name):
        unknown_storage = path.join(self.storage, 'unknown')
        return path.join(unknown_storage, name)

    def load_user_by_index_key(self, index_key=0):
        key_str = str(index_key)
        if key_str in self.face_user_keys:
            return self.face_user_keys[key_str]
        return None

    def loadAll(self):
        for filename in os.listdir(self.directory):
            self.filenames.append(filename)
            print(self.filenames)
            filename = os.path.join(self.directory, filename)
            image = face_recognition.load_image_file(filename)
            face_locations = face_recognition.face_locations(image)
            known_encoding = face_recognition.face_encodings(image)[0]
            self.known_encoding_faces.append(known_encoding)

    def recognize(self, unknown_filename,logged_lecturer):
        self.loadAll()
        unknown_image = face_recognition.load_image_file(self.load_unknown_file_by_name(unknown_filename))
        unknonwn_location = face_recognition.face_locations(unknown_image, number_of_times_to_upsample=2)
        unknown_encoding = face_recognition.face_encodings(unknown_image, unknonwn_location)
        self.identified_face=len(unknown_encoding)
        print(len(unknown_encoding))
        pil_image = Image.fromarray(unknown_image)
        # Create a ImageDraw instance
        draw = ImageDraw.Draw(pil_image)

        # Loop through faces in test image
        for (top, right, bottom, left), face_encoding in zip(unknonwn_location, unknown_encoding):
            matches = face_recognition.compare_faces(self.known_encoding_faces, face_encoding, tolerance=0.45)
            print(matches)
            name = ''

            # If match
            if True in matches:
                first_match_index = matches.index(True)
                name = self.filenames[first_match_index]
                name = name[:-4]
                now = datetime.now()
                formatted_date = now.strftime('%Y-%m-%d')
                print(formatted_date)
                insert_attendance=""" INSERT INTO attendance(student_index_number,lecturer_id,state,date_taken) SELECT %s,%s,%s,%s FROM  DUAL WHERE NOT EXISTS(SELECT * FROM attendance WHERE student_index_number=%s AND date_taken=%s)
 """
                # attendancequery = """INSERT INTO attendance(student_index_number, lecturer_id, state, date_taken) VALUES (%s,%s,%s,%s) ON DUPLICATE KEY UPDATE student_index_number=%s"""
                student_database.insert(insert_attendance, [name, logged_lecturer, "Present", formatted_date, name,formatted_date])
            else:
                print("Invalid Record")
                return -1
            # Draw box
            draw.rectangle(((left, top), (right, bottom)), outline=(57, 204, 49))
            # Draw label
            text_width, text_height = draw.textsize(name)
            draw.rectangle(((left, bottom - text_height - 10), (right, bottom)), fill=(57, 204, 49),
                           outline=(57, 204, 49))
            draw.text((left + 6, bottom - text_height - 5), name, fill=(255, 255, 255))
        del draw

        # Display image
        pil_image.show()
        pil_image.save("../facedetectionmodel/static/img/model_complete.jpg",'JPEG')
        return self.identified_face




        # Save imagepil_image.save(name)
