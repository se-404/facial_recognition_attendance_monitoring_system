from PIL import Image
import face_recognition
import os

imagepath="insert_image_path_here"
image = face_recognition.load_image_file(imagepath)
face_locations = face_recognition.face_locations(image)
print("I found {} face(s) in this photograph.".format(len(face_locations)))
for number in range(len(face_locations)):
    face_cap = image
    top, right, bottom, left = face_locations[number]
    face_image = face_cap[top:bottom, left:right]
    pil_image = Image.fromarray(face_image)
    file_name = './Sat/image_%d.jpg'%number
    pil_image.save(file_name)

